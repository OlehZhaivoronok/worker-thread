import faker from 'faker';
import { cpus } from 'os';
import path from 'path';
import { WorkerQueues } from './workerQueues';

const numCPUs = cpus().length;
const options = {
  maxWorkers: numCPUs,
  maxHangupDuration: 10000,
  maxErrorRetryCount: 3
};
const testItemsCount = 20;

const workerQueues = new WorkerQueues<number>(path.join(__dirname, './worker.js'), options);

const items = [...new Array(testItemsCount)].fill(undefined);

Promise.all(
  items.map(async (_el: undefined, i: number) => {
    try {
      const res = await workerQueues.run(() => ({ i, method: 'hash', data: faker.random.alphaNumeric(faker.random.number(2000)) }));
      console.info('finished', i, res);
    } catch (err) {
      console.error('job error', err, i);
    }
  }),
).finally(() => {
  console.log('finished all', workerQueues.queueItems.length);
  process.exit(1);
});
