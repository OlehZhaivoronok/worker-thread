import { isMainThread, MessagePort, parentPort } from 'worker_threads';
import faker from 'faker';
import crypto from 'crypto';
import { JobData } from './workerQueues';

if (isMainThread) {
  throw new Error('Its not a worker');
}
if (!parentPort) {
  throw new Error('Parent port is missing');
}

const messagePort = parentPort as MessagePort;
const makeHash = (str: string, i: number): Promise<string> => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const emulateError = faker.random.number({ min: 0, max: 10, precision: 1 }) === 0;
      if (emulateError) {
        console.info('emulated error', i);
        reject(new Error('Test error: ' + i));
      }
      const hash = crypto.createHash('sha256');
      resolve(hash.copy().update(str).digest('hex'));
    }, faker.random.number(2000));
  });
};

messagePort.on('message', async (req: JobData) => {
  try {
    const emulateHangup = faker.random.number({ min: 0, max: 10, precision: 1 }) === 0;
    if (emulateHangup) console.info('emulated hangup', req.i);
    else if (req.method === 'hash') {
      const result = await makeHash(req.data as string, req.i);
      messagePort.postMessage(result);
    }
  } catch (error) {
    messagePort.postMessage({ error });
  }
});
