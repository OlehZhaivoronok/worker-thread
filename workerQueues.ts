import { Worker } from 'worker_threads';

export interface JobData {
  i: number;
  method: string;
  data: unknown;
}

interface WorkerOptions {
  maxWorkers: number;
  maxHangupDuration: number;
  maxErrorRetryCount: number;
}

type QueueCallback<T> = (err: Error | undefined, result?: T) => void;
interface QueueItem<T> {
  callback: QueueCallback<T>;
  getData: () => JobData;
}

export class WorkerQueues<T> {
  public queueItems: QueueItem<T>[] = [];
  private readonly workersById: Record<number, Worker> = {};
  private readonly activeWorkersById: Record<number, boolean> = {};
  public constructor(private readonly workerPath: string, private readonly opt: WorkerOptions) {
    if (this.opt.maxWorkers > 1) {
      for (let i = 0; i < this.opt.maxWorkers; i += 1) {
        const worker = new Worker(this.workerPath);
        this.workersById[i] = worker;
        this.activeWorkersById[i] = false;
      }
    }
  }

  public run(getData: () => JobData): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      const queueItem: QueueItem<T> = {
        getData,
        callback: (error, result) => {
          if (error) {
            return reject(error);
          }
          return resolve(result);
        },
      };
      const availableWorkerId = this.getInactiveWorkerId();
      if (availableWorkerId === -1) this.queueItems.push(queueItem);
      else this.evaluateJob(availableWorkerId, queueItem);
    });
  }

  private getInactiveWorkerId(): number {
    for (let i = 0; i < this.opt.maxWorkers; i++) {
      if (!this.activeWorkersById[i]) return i;
    }
    return -1;
  }

  private checkQueue(workerId: number): void {
    this.activeWorkersById[workerId] = false;
    if (!this.queueItems.length) return;
    const awaitingJob = this.queueItems.shift();
    if (awaitingJob) this.evaluateJob(workerId, awaitingJob);
  }

  private async evaluateJob(workerId: number, queueItem: QueueItem<T>): Promise<void> {
    this.workersById[workerId];
    this.activeWorkersById[workerId] = true;
    const data = queueItem.getData();
    try {
      console.info('started', (data as { i: number }).i);
      const result = await this.postMessageWithRetry(workerId, data);
      queueItem.callback(undefined, result);
      this.checkQueue(workerId);
    } catch (error) {
      queueItem.callback(error);
      this.checkQueue(workerId);
    }
  }

  private rejectMessage(timer: NodeJS.Timeout, error: Error, callback: (reason?: Error) => void): void {
    clearTimeout(timer);
    callback(error);
  }

  private async postMessageWithRetry(workerId: number, data: JobData): Promise<T> {
    for (let index = 0; index < this.opt.maxErrorRetryCount; index++) {
      try {
        return await this.postMessage(workerId, data);
      } catch (error) {}
    }

    throw new Error('Job error: ' + data.i);
  }

  private async postMessage(workerId: number, data: JobData): Promise<T> {
    const worker = this.workersById[workerId];
    worker.removeAllListeners('message');
    worker.removeAllListeners('error');
    return new Promise((resolve, reject) => {
      const timer = setTimeout(async () => {
        await worker.terminate();
        this.workersById[workerId] = new Worker(this.workerPath);
        this.rejectMessage(timer, new Error('Error hangup duration: ' + data.i), reject);
      }, this.opt.maxHangupDuration);

      worker.once('message', (message) => {
        if (message.error) this.rejectMessage(timer, message.error, reject);
        else {
          clearTimeout(timer);
          resolve(message);
        }
      });

      worker.once('error', (error) => {
        this.rejectMessage(timer, error, reject);
      });

      worker.postMessage(data);
    });
  }
}
