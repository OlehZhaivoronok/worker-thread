The application evaluates a hash for strings (with a simulated delay up to 2 sec) in a separated thread and has has the following parameters (you can find this in the file "main.ts"):

    const numCPUs = cpus().length;      // count of CPUs

    const options = {
        maxWorkers: numCPUs,            // limit for workers count, can be or number, or numCPUs
        maxHangupDuration: 10000,       // time in milliseconds to check case of hangup 
        maxErrorRetryCount: 3           // max retry count for an error
    };
    const testItemsCount = 20;          // size for a mock data for a test


steps to start
 - npm i
 - npm run debug // lunch the application in watch mode, so you can re-save any file and watch new messages

 possible messages:
  - started N - started a job in a thread number N
  - finished N HASH - finished a job in a thread number N with a result HASH
  - emulated hangup N - emulated a hangup in a thread number N. Probability of error is 1/20
  - emulated error N - emulated an error in a thread number N. Probability of error is 1/20
  - finished all N - all job done and queue length is N (must be always zero)

